function getAllItems() {
    fetch('http://localhost:8080/items')
  .then((response) => {
    return response.json();
  })
  .then((data) => {
    showAllItems(data);
  });
}

function getActiveItems() {
    fetch('http://localhost:8080/items')
  .then((response) => {
    return response.json();
  })
  .then((data) => {
    showActiveItems(data);
  });
}

function getCompletedItems() {
    fetch('http://localhost:8080/items')
  .then((response) => {
    return response.json();
  })
  .then((data) => {
    showCompletedItems(data);
  });
}

function addItems(item) {
    fetch('http://localhost:8080/items', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(item),
    })
  .then((response) => {
    return response.json();
  })
  .then(() => getAllItems());
}

function updateItems(item) {
    fetch('http://localhost:8080/items', {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(item),
    })
  .then((response) => {
    return response.json();
  })
  .then(() => getAllItems());
}

function deleteItems(item) {
    fetch('http://localhost:8080/items' + '/' + item, {
        method: 'DELETE',
    })
  .then(() => getAllItems());
}