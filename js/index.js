// Need to be implement
function generateItem(item) {
    return '<li class="' + item.status + '"><input type="checkbox" ' + (item.status === 'COMPLETED' ? 'checked ' : ' ') 
        + 'onclick="changeItemStatus(' + item.id + ', \'' + item.text + '\', \'' + item.status + '\')"><span id="' + item.id 
        + '" onclick="makeEditable(this)" onkeydown="editItem(this, \'' + item.status + '\')" onmouseout="makeDisEditable(this)">' 
        + item.text + '</span><button class="deleteButton" onclick="deleteItem(' + item.id + ')"></button></li>';
}

function showAllItems(data) {
    let items = data.map(item => generateItem(item)).reduce((a, b) => a + b, '');
    document.getElementById('items').innerHTML = items;
    document.getElementById('footerButtonAll').setAttribute('class', 'footerButton-checked');
    document.getElementById('footerButtonActive').setAttribute('class', 'footerButtons-unchecked');
    document.getElementById('footerButtonCompleted').setAttribute('class', 'footerButtons-unchecked');
}

function showActiveItems(data) {
    let items = data.filter(item => item.status === 'ACTIVE')
    .map(item => generateItem(item)).reduce((a, b) => a + b, '');
    document.getElementById('items').innerHTML = items;
    document.getElementById('footerButtonAll').setAttribute('class', 'footerButtons-unchecked');
    document.getElementById('footerButtonActive').setAttribute('class', 'footerButton-checked');
    document.getElementById('footerButtonCompleted').setAttribute('class', 'footerButtons-unchecked');
}

function showCompletedItems(data) {
    let items = data.filter(item => item.status === 'COMPLETED')
    .map(item => generateItem(item)).reduce((a, b) => a + b, '');
    document.getElementById('items').innerHTML = items;
    document.getElementById('footerButtonAll').setAttribute('class', 'footerButtons-unchecked');
    document.getElementById('footerButtonActive').setAttribute('class', 'footerButtons-unchecked');
    document.getElementById('footerButtonCompleted').setAttribute('class', 'footerButton-checked');
}

function addToDoItem() {
    let inputItem = document.getElementById('inputItem').value;
    let item = {
        text: inputItem,
    };
    document.getElementById('inputItem').value = '';
    addItems(item);
    
    if (document.getElementById('footerButtonAll').getAttribute('class') === 'footerButton-checked') {
        getAllItems();
    } else if (document.getElementById('footerButtonActive').getAttribute('class') === 'footerButton-checked') {
        getActiveItems();
    } else if (document.getElementById('footerButtonCompleted').getAttribute('class') === 'footerButton-checked') {
        getCompletedItems();
    }
}

function enterItem(key) {
    if (key.keyCode === 13) {
        addToDoItem();
    }
}

function changeItemStatus(changeId, changeText, changeStatus) {
    let items = {
        id: changeId,
        text: changeText,
        status: (changeStatus === 'COMPLETED' ? 'ACTIVE' : 'COMPLETED')
    };
    updateItems(items);

    if (document.getElementById('footerButtonAll').getAttribute('class') === 'footerButton-checked') {
        getAllItems();
    } else if (document.getElementById('footerButtonActive').getAttribute('class') === 'footerButton-checked') {
        getActiveItems();
    } else if (document.getElementById('footerButtonCompleted').getAttribute('class') === 'footerButton-checked') {
        getCompletedItems();
    }
}

function makeEditable(content) {
    content.setAttribute('contenteditable', true);
}

function makeDisEditable(content) {
    content.setAttribute('contenteditable', false);
}

function editItem(content, editStatus) {
    if (event.keyCode === 13) {
        let newContent = content.innerText;
        let selectedId = content.getAttribute('id');
        makeDisEditable(content);
        let items =  {
            id: parseInt(selectedId),
            text: newContent,
            status: editStatus
        };
        updateItems(items);
    }
}

function deleteItem(id) {
    deleteItems(id);
}

